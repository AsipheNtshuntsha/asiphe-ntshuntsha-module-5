import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class ReadList extends StatefulWidget {
  const ReadList({Key? key}) : super(key: key);

  @override
  State<ReadList> createState() => _ReadListState();
}

class _ReadListState extends State<ReadList> {
  final Stream<QuerySnapshot> _myReadingList =
      FirebaseFirestore.instance.collection('Account').snapshots();

  @override
  Widget build(BuildContext context) {
    TextEditingController _createPasswordFieldCtrlr = TextEditingController();
    TextEditingController _usernameFieldCtrlr =TextEditingController();

    void _delete(docId) {
      FirebaseFirestore.instance
          .collection("Account")
          .doc(docId)
          .delete()
          .then((value) => print("deleted"));
    }

    void _update(data) {
      var collection = FirebaseFirestore.instance.collection("Account");

      _createPasswordFieldCtrlr.text = data["Create_password"];
      _usernameFieldCtrlr.text = data["Your_username"];

      showDialog(
          context: context,
          builder: (_) => AlertDialog(
                title: const Text("Update"),
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    TextField(
                      controller: _createPasswordFieldCtrlr,
                    ),
                    TextField(
                      controller: _usernameFieldCtrlr,
                    ),

                    TextButton(
                      onPressed: () {
                        collection.doc(data["doc_id"])
                        .update({
                          "Create_password":
                              _createPasswordFieldCtrlr.text,
                          "Your_username": _usernameFieldCtrlr.text,
                        });
                        Navigator.pop(context);
                      },
                      child: const Text("Update"),
                    )
                  ],
                ),
              ));
    }

    return StreamBuilder(
      stream: _myReadingList,
      builder: (BuildContext context,
          AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
        if (snapshot.hasError) {
          return const Text("Something went wrong");
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return const CircularProgressIndicator();
        }

        if (snapshot.hasData) {
          return Row(
            children: [
              Expanded(
                  child: SizedBox(
                      height: (MediaQuery.of(context).size.height),
                      width: (MediaQuery.of(context).size.width),
                      child: ListView(
                        children: snapshot.data!.docs
                            .map((DocumentSnapshot documentSnapshot) {
                          Map<String, dynamic> data =
                              documentSnapshot.data()! as Map<String, dynamic>;

                          return Column(children: [
                            Card(
                              child: Column(
                                children: [
                                  ListTile(
                                    title: Text(data["Create_password"]),
                                    subtitle: Text(data["Your_username"]),
                                  ),
                                  ButtonTheme(
                                      child: ButtonBar(
                                    children: [
                                      OutlinedButton.icon(
                                        onPressed: () {
                                          _update(data);
                                        },
                                        icon: const Icon(Icons.edit),
                                        label: const Text("Edit"),
                                      ),
                                      OutlinedButton.icon(
                                        onPressed: () {
                                          _delete(data["doc_id"]);
                                        },
                                        icon: const Icon(Icons.remove),
                                        label: const Text("Delete"),
                                      )
                                    ],
                                  ))
                                ],
                              ),
                            )
                          ]);
                        }).toList(),
                      )))
            ],
          );
        } else {
          return (const Text("No data"));
        }
      },
    );
  }
}
